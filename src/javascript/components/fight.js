export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstPlayer = {...firstFighter, defencePower: 0, totalDamage: 0, cooldown: false};
    const secondPlayer = {...secondFighter, defencePower: 0, totalDamage: 0, cooldown: false};
    const firstPlayerHelthIndicator = document.getElementById('left-fighter-indicator');
    const secondPlayerHelthIndicator = document.getElementById('right-fighter-indicator');
    const validKeys = ['a', 'd', 'j', 'l', 'q', 'w', 'e', 'u', 'i', 'o']

    const firstPlayerCombination = ['q', 'w', 'e'];
    const secondPlayerCombination = ['u', 'i', 'o'];
    const currentFirstPlayerKeys = [];
    const currentsecondPlayerKeys = [];

    const handleOnKeyDown = (ev) => {
      const key = ev.key.toLowerCase();
      
      if (validKeys.includes(key)) {
        if (firstPlayerCombination.includes(key)) {
          checkCrit(firstPlayerCombination, currentFirstPlayerKeys, key, firstPlayer, secondPlayer, secondFighter.health, secondPlayerHelthIndicator, firstPlayer);
        } else if (secondPlayerCombination.includes(key)) {
          checkCrit(secondPlayerCombination, currentsecondPlayerKeys, key, secondPlayer, firstPlayer, firstFighter.health, firstPlayerHelthIndicator, firstPlayer);
        } else {
          checkAttack(key, firstPlayer, secondPlayer, firstFighter.health, secondFighter.health, firstPlayerHelthIndicator, secondPlayerHelthIndicator);
          checkDefence(key, firstPlayer, secondPlayer);
        }
      }
    };

    const handleOnKeyUp = (ev) => {
      const key = ev.key.toLowerCase();
      const winner = checkWin(firstPlayer, secondPlayer, resolve);

      if (winner) {
        document.removeEventListener('keydown', handleOnKeyDown);
        document.removeEventListener('keyup', handleOnKeyUp);

        resolve(winner);
      }
      removeDefence(key, firstPlayer, secondPlayer);
      if (firstPlayerCombination.includes(key, firstPlayer, secondPlayer)) {
        removeCombinationKey(key, currentFirstPlayerKeys);
      } else if (secondPlayerCombination.includes(key)) {
        removeCombinationKey(key, currentsecondPlayerKeys);
      }
    }

    document.addEventListener('keydown', handleOnKeyDown);
    document.addEventListener('keyup', handleOnKeyUp);
  });
}

export function getDamage(attacker, defender) {
  try {
    const attack = getHitPower(attacker);

    if (attack > defender.defencePower) {
      return attack - defender.defencePower;
    }

    return 0;
  } catch (err) {
    throw err;
  }
}

export function getHitPower(fighter) {
  try {
    return fighter.attack * dodgeChance();
  } catch (err) {
    throw err;
  }
}

export function getBlockPower(fighter) {
  try {
    return fighter.defense * dodgeChance();
  } catch (err) {
    throw err;
  }
}

const dodgeChance = () => 1 + Math.random();

const checkAttack = (key, firstPlayer, secondPlayer, firstFighterTotalHelth, secondFighterTotalHelth, firstPlayerHelthIndicator, secondPlayerHelthIndicator) => {
  const isFirstPlayerAttack = key === 'a';
  const isSecoundPlayerAttack = key === 'j';

  if (isFirstPlayerAttack || isSecoundPlayerAttack) {

    if (isFirstPlayerAttack && !firstPlayer.defencePower) {
      setAttack(firstPlayer, secondPlayer, secondFighterTotalHelth, secondPlayerHelthIndicator, firstPlayer);
    } else if (isSecoundPlayerAttack && !secondPlayer.defencePower) {
      setAttack(secondPlayer, firstPlayer, firstFighterTotalHelth, firstPlayerHelthIndicator, firstPlayer);
    }

  };
}

const setAttack = (attacker, defender, defenderTotalHelth, defenderIndicator, firstPlayer, isStandartAttack = true, customAttack = 0) => {
  const attack = isStandartAttack ? getDamage(attacker, defender) : customAttack;

  const attackerPosition = firstPlayer.name === attacker.name ? 'left' : 'right';
  const attackerImage = document.querySelector(`div.arena___fighter.arena___${attackerPosition}-fighter > img`);

  attackerImage.classList.add(`fighter-attack-${attackerPosition}`);
  attackerImage.src = `resources/animation/${attacker.name}_attack.gif`;

  setTimeout(() => {
    attackerImage.classList.remove(`fighter-attack-${attackerPosition}`);
    attackerImage.src = attacker.source;
  }, 3000)

  if (attack) {
    defender.health = defender.health - attack;
    defender.totalDamage = defender.totalDamage + attack;

    updateHelthIndicator(defenderIndicator, defenderTotalHelth, defender.totalDamage);
  }
}

const updateHelthIndicator = (indicator, totalHelth, totalDamage) => {
  const percent = totalHelth <= totalDamage ? '0' : `${100 - ((totalDamage * 100) / totalHelth)}%`;

  indicator.style.width = percent;
}

const checkDefence = (key, firstPlayer, secondPlayer) => {
  const isFirstPlayerDefence = key === 'd';
  const isSecoundPlayerDefence = key === 'l';

  if (isFirstPlayerDefence || isSecoundPlayerDefence) {
    if (isFirstPlayerDefence) {
      setDefence(firstPlayer);
    } else if (isSecoundPlayerDefence) {
      setDefence(secondPlayer);
    }
  };
}

const setDefence = (player) => player.defencePower = getBlockPower(player)

const removeDefence = (key, firstPlayer, secondPlayer) => {
  const isFirstPlayerDefence = key === 'd';
  const isSecoundPlayerDefence = key === 'l';

  if (isFirstPlayerDefence) {
    firstPlayer.defencePower = 0;
  } else if (isSecoundPlayerDefence) {
    secondPlayer.defencePower = 0;
  }
}

const checkCombinationKeys = (combination, currentKeys) => {
  const result = combination.map((el) => currentKeys.includes(el) ? 1 : 0);

  return !result.includes(0);
}

const checkCrit = (combination, currentKeys, key, attacker, defender, defenderTotalHelth, defenderIndicator, firstPlayer) => {
  currentKeys.push(key);
  const isCritical = checkCombinationKeys(combination, currentKeys);
  
  if (isCritical && !attacker.cooldown) {
    attacker.cooldown = true;
    setInterval(() => {
      attacker.cooldown = false;
    }, 10000);
    setAttack(attacker, defender, defenderTotalHelth, defenderIndicator, firstPlayer, false, attacker.attack * 2);
  }
}

const removeCombinationKey = (key, combination) => {
  const keyIndex = combination.indexOf(key);

  if (keyIndex !== -1) {
    combination.splice(keyIndex, 1);
  }
}

const checkWin = (firstPlayer, secondPlayer, resolve) => {
  if (firstPlayer.health <= 0 || secondPlayer.health <= 0) {
    return firstPlayer.health >= 0 ? firstPlayer : secondPlayer
  }

  return false;
}