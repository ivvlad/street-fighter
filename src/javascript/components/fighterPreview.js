import { createElement } from '../helpers/domHelper';
import { fight } from './fight';

export function createFighterPreview(fighter, position) {
  const isRight = position === 'right';
  const positionClassName = isRight ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} ${isRight ? 'align-end' : 'align-start'}`,
  });

  if(fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterName = createElement({tagName: "h3", className: 'fighter-preview__name'})
    const fighterDetails = createElement({tagName: "ul", className: 'fighter-preview__details'})
    const fighterInfoWrapper = createElement({tagName: "div", className: `fighter-preview_wrapper ${isRight ? 'fighter-preview_wrapper___right' : 'fighter-preview_wrapper___left'}`})
  
    fighterName.innerHTML = fighter.name;
    fighterDetails.innerHTML = `
      <li class="details__item ${isRight ? 'self-end text-right' : 'self-start'}">
        ${isRight ? `<p>${fighter.health}</p><img src="resources/heart.png" />` : `<img src="resources/heart.png" /><p>${fighter.health}</p>`}
      </li>
      <li class="details__item ${isRight ? 'self-end text-right' : 'self-start'}">
        ${isRight ? `<p>${fighter.defense}</p><img src="resources/shield.png" />` : `<img src="resources/shield.png" /><p>${fighter.defense}</p>`}
      </li>
      <li class="details__item ${isRight ? 'self-end text-right' : 'self-start'}">
        ${isRight ? `<p>${fighter.attack}</p><img src="resources/arm.png" />` : `<img src="resources/arm.png" /><p>${fighter.attack}</p>`}
      </li>
    `;
  
    fighterInfoWrapper.append(fighterDetails, fighterName)
    fighterElement.append(fighterImage, fighterInfoWrapper)
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
